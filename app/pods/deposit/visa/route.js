import DepositMethodRoute from '../../deposit/method/route';

export default DepositMethodRoute.extend({
    setupController: function(controller, model) {
        this._super(controller, model);
        controller.set('title', 'Visa Deposit');
    }
});

