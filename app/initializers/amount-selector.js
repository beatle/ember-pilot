import amountSelector from "../components/slider-amount-selector";
export default {
	name: 'amount-selector',
	initialize: function(container, application) {
		application.register('component:amount-selector', amountSelector); 
	}
};
