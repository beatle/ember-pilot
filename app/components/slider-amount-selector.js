import Ember from 'ember';

export default Ember.Component.extend({
    layoutName:'components/slider-amount-selector',
    
    actions: {
        setAmount: function(amount) {
            this.set('amount', amount);
        }
    }
});

